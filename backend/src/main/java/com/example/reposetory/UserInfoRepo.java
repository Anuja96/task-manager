package com.example.reposetory;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.entity.UserInfo;

@Repository
public interface UserInfoRepo extends CrudRepository<UserInfo, String> {

}
