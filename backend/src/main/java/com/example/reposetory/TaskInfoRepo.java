package com.example.reposetory;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.entity.TaskInfo;
@Repository
public interface TaskInfoRepo extends CrudRepository<TaskInfo, String> {

}
