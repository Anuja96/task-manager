package com.example.mycontroller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.TaskInfo;
import com.example.entity.UserInfo;
import com.example.pojo.LoginResponce;
import com.example.service.TaskInfoService;
import com.example.service.UserInfoService;




@RestController
public class MyController {
	@Autowired
	UserInfoService userInfo;
	
	@Autowired
	TaskInfoService taskInfo;
	
	@GetMapping("/hello")
	public String getHello() {
		return"Hiiii";
	}
	
	
	@GetMapping("/login")
	public LoginResponce setLogin(@RequestParam String username,@RequestParam String password) {
		List<UserInfo> detals=userInfo.getAllUserInfo();	
		LoginResponce loginResponce=new LoginResponce();
		for(int i=0;i<detals.size();i++) {
			if(username.equals(detals.get(i).getUserName())) {
				if(password.equals(detals.get(i).getPassword())) {
					loginResponce.setIsSucess(true);
					loginResponce.setMsg("Login Sucessfully");
					loginResponce.setUserId(detals.get(i).getUserId());
					/*
					 * if(!fcmId.isEmpty()) { TeamDetal detal12=new TeamDetal();
					 * detal12=detals.get(i); detal12.setFcmId(fcmId); setPlayer(detal12); }
					 */
					break;
				}else {
					loginResponce.setIsSucess(false);
					loginResponce.setMsg("Wrong Password");
					loginResponce.setUserId(00);
					break;
		}
				
			}else if(i==detals.size()-1) {
				loginResponce.setIsSucess(false);
				loginResponce.setMsg("User Not Exist");
				loginResponce.setUserId(00);
				
			}
		}
		return loginResponce;
		
		
	}
	
	
	
	
	@GetMapping("/getalluser")
	public List<UserInfo> allPlayerData() {
	List<UserInfo> detals=userInfo.getAllUserInfo();	
	return detals;
	}
	
	@PostMapping("/setTask")
	public String addTask(@RequestParam String taskname, @RequestParam String startdate, 
			@RequestParam String enddate, @RequestParam String taskdesc, @RequestParam String taskstatus,
			@RequestParam String taskid) {
		if(taskid.equals("0"))
		{
			TaskInfo task=new TaskInfo();
			task.setTaskName(taskname);
			task.setStartDate(startdate);
			task.setEndDate(enddate);
			task.setTaskDescripton(taskdesc);
			task.setTaskStatus(taskstatus);
			System.out.println();
			String s=taskInfo.insertTask(task);
			return s;
		}
		else
		{
			List<TaskInfo> detals=taskInfo.getAllTask();
			for(int i=0;i<detals.size();i++)
			{
				if(detals.get(i).getTaskId() == Integer.parseInt(taskid))
				{
					detals.get(i).setTaskId(Integer.parseInt(taskid));
					detals.get(i).setTaskName(taskname);
					detals.get(i).setStartDate(startdate);
					detals.get(i).setEndDate(enddate);
					detals.get(i).setTaskDescripton(taskdesc);
					detals.get(i).setTaskStatus(taskstatus);
					String s=taskInfo.insertTask(detals.get(i));
					return s;	
				}
			}
			
			return "true";
			
		}
	}
		
	
	@GetMapping("/getalltask")
	public List<TaskInfo> allTaskData() {
	List<TaskInfo> detals=taskInfo.getAllTask();	
	return detals;
	}
	
	@SuppressWarnings("null")
	@GetMapping("/gettaskbystatus")
	public List<TaskInfo> taskDataByStatus(@RequestParam String status) {
		System.out.println(status);
	List<TaskInfo> detals=taskInfo.getAllTask();
	List<TaskInfo> filterdetails = new ArrayList<TaskInfo>();
	for(int i=0;i<detals.size();i++)
	{
		if(detals.get(i).getTaskStatus().equals(status))
		{
			filterdetails.add(detals.get(i));
		}
		}
	return filterdetails;
	}
	
}
