package com.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="team_detal")
public class TeamDetal {
	@Column (name="player_name")
	String playerName;
	@Id
	@Column (name="player_id")
	String id;
	@Column (name = "mobile_number")
	String mobileNumber;
	@Column(name="player_type")
	String playerType;
	@Column(name="total_score")
	String totalScore;
	@Column(name="password")
	String password;
	@Column(name="email_id")
	String emailId;
	@Column(name = "fcm_id")
	String fcmId;
	
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getFcmId() {
		return fcmId;
	}
	public void setFcmId(String fcmId) {
		this.fcmId = fcmId;
	}
	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getPlayerType() {
		return playerType;
	}
	public void setPlayerType(String playerType) {
		this.playerType = playerType;
	}
	public String getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(String totalScore) {
		this.totalScore = totalScore;
	}
	
	

}
