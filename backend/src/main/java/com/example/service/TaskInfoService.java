package com.example.service;

import java.util.List;
import java.util.Optional;

import com.example.entity.TaskInfo;



public interface TaskInfoService {
	List<TaskInfo> getAllTask();
	Optional<TaskInfo> getSingleTaskById(String id);
	String insertTask(TaskInfo info);

	//UserInfo checkLogin(String userName, String pass);
}
