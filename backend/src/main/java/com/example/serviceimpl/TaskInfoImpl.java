package com.example.serviceimpl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.TaskInfo;
import com.example.entity.UserInfo;
import com.example.reposetory.TaskInfoRepo;
import com.example.reposetory.UserInfoRepo;
import com.example.service.TaskInfoService;

@Service
public class TaskInfoImpl implements TaskInfoService {

	@Autowired
	TaskInfoRepo repo;
	@Override
	public List<TaskInfo> getAllTask() {
		List<TaskInfo> info=(List<TaskInfo>) repo.findAll();
		return info;
	}

	@Override
	public Optional<TaskInfo> getSingleTaskById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String insertTask(TaskInfo info) {
//		repo.save(info);
//		return "userinfoadded";
//		TaskInfo task = new TaskInfo();

		
		repo.save(info);
		System.out.println(info.getTaskName());
        return "userinfoadded";
	}

}
