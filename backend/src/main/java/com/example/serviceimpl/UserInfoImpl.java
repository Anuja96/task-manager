package com.example.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.UserInfo;
import com.example.reposetory.UserInfoRepo;
import com.example.service.UserInfoService;



@Service
public class UserInfoImpl implements UserInfoService {

	@Autowired
	UserInfoRepo repo;
	@Override
	public List<UserInfo> getAllUserInfo() {
		List<UserInfo> info=(List<UserInfo>) repo.findAll();
		return info;
	}

	@Override
	public Optional<UserInfo> getSingleLeaveApplication(String id) {
		Optional<UserInfo> info=repo.findById(id);
		return info;
	}

	@Override
	public String insertUserInfo(UserInfo info) {
		repo.save(info);
		return "userinfoadded";
	}

}
